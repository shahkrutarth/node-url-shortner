const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UrlSchema = new Schema({
	_id: { type: Number, required: true },
    url: { type: String },
    expiryOn: {type: Date, default: null},
    createdOn: {type: Date, default: Date.now}
}, { collection: 'shorturl' });

const ShortUrl = mongoose.model('shorturl', UrlSchema);

module.exports = {
	urlExist: function(url) {
		return new Promise(resolve => {
            ShortUrl.find({'url': url})
            .then(function (data) {
            	resolve(data)
            });
        });
	},
	createRecord: function(insertData) {
		return new Promise(resolve => {
            ShortUrl.create(insertData)
            .then(function (data) {
            	resolve(data)
            });
        });
	},
	getUrlById: function(countId) {
        return new Promise(resolve => {
            ShortUrl.findOne({'_id': countId})
            .then(function (data) {
                resolve(data)
            });
        });
	}
};