const _ = require('underscore');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CounterSchema = new Schema({
	_id: { type: String, required: true },
    count: { type: Number, default: 1000000001 }
}, { collection: 'counter' });

const Counter = mongoose.model('Counter', CounterSchema);


module.exports = {
	getURLSequence: function(sequenceName) {
        if (_.isUndefined(sequenceName)) {
            sequenceName = 'url-shortner';
        }
		return new Promise(resolve => {
            Counter.findByIdAndUpdate({_id: sequenceName}, {$inc: { count: 1} })
            .then(function (data) {
            	resolve(data)
            });
        });
	}
};