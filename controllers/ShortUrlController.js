const ShortUrlModel = require('../models/shorturl.js');
const ENV = require('dotenv').config()
const Counter = require('../models/counter.js');
const moment = require('moment');
const _ = require('underscore');
const validUrl = require('valid-url');
const unhash = require('atob');
const hash = require('btoa');

module.exports = {
	hashUrl: function(counter) {
		let newUrl = 'http://'+ENV.parsed.SHORT_URL_DOMAIN_NAME;
		if (ENV.parsed.IS_HTTPS) {
			newUrl = 'https://'+ENV.parsed.SHORT_URL_DOMAIN_NAME;
		}
		counter = String(counter);
		newUrl += hash(counter);
		return newUrl;
	},
	unhashUrl: async function(url) {
		let urlData = url.split('/');
		let counterId = Number(unhash(_.last(urlData)));
		let counterData = await ShortUrlModel.getUrlById(counterId);
		let expDate = moment(counterData.expiryOn);
		let now = moment().format("DD.MM.YYYY H:m:s");

		expDate = expDate.utc().format("DD.MM.YYYY H:m:s");
		expDate = moment(expDate, "DD.MM.YYYY H:m:s");
		now = moment(now, "DD.MM.YYYY H:m:s");

		let expTime = expDate.diff(now, 'seconds');

		if (expTime > 0) {
			return counterData.url;
		} else {
			return false;
		}
	},
	shorten: async function(req, res) {
		let actualUrl = req.body.url;
		let errorMsg = 'Please enter URL in format http://YOURDOMAINNAME.COM';
		let responseData = {};

		if (validUrl.isWebUri(actualUrl)) {
			let checkURL = await ShortUrlModel.urlExist(actualUrl);

			if (_.isEmpty(checkURL)) {
				// CREATE NEW URL MISSING IN DB
				let counterData = await Counter.getURLSequence('url-shortner');
				let counterId = counterData.count;
				let expiryDate = moment().add(ENV.parsed.DEFAULT_EXPIRY, 'days');
				let storeUrl =  {
									'_id': counterId,
									'url': actualUrl,
									'expiryOn': expiryDate
								};
				await ShortUrlModel.createRecord(storeUrl);
				let newUrl = this.hashUrl(counterId);
				return res.json({ status: 'success', message: 'Your new URL is ready to use and expires on ' + expiryDate, data: {'url': newUrl} });
			} else {
				// RETURN HASH SHORTEN URL | URL ALREADY EXIST
				let newUrl = this.hashUrl(checkURL._id);
				return res.json({ status: 'success', message: 'Your new URL is ready to use and expires on ' + checkURL.expiryOn, data: {'url': newUrl} });
			}
			// USE THIS COUNTER ID FOR HASHING AND STORE ACTUAL URL AGAINST THIS ID IN URL SHORTNER COLLECTION
		} else {
			return res.json({ status: 'error', message: errorMsg, data: {} });
		}
	},
	getActualUrl: async function(req, res) {
		let URL = '';
		if (!req.params.url) {
			/* API CALL */
			URL = await this.unhashUrl(req.body.url);
			if (URL) {
				return res.json({ status: 'success', message: 'Your redirect URL', data: {'url': URL} });
			} else {
				return res.json({ status: 'error', message: 'Invalid/Expired URL', data: {} });
			}
		} else {
			/* WEB CALL */
			URL = await this.unhashUrl(req.params.url);
			if (URL) {
				res.redirect(URL);
			} else {
				// return 404
			}
		}
	}
}