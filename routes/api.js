const path = require('path');
const express = require('express');

const UrlController = require('../controllers/ShortUrlController');

module.exports = function(app){
	'use strict';

    let router = express.Router();

	router.get('/', function (req, res) {
		res.send('I am here');
	});
	router.post('/shortner', function (req, res) {
		UrlController.shorten(req, res);
	});
	router.post('/url', function (req, res) {
		UrlController.getActualUrl(req, res);
	});

	app.use('/api/v1', router);
}