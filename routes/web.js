const path = require('path');

const UrlController = require('../controllers/ShortUrlController');

module.exports = function(app){
	app.get('/:url?', function (req, res) {
		let key = req.params.url;
		if (!key) {
			res.sendFile(path.join(__dirname + '/../public/index.html'));
			return;
		} else {
			UrlController.getActualUrl(req, res);
		}
	});

	app.post('/shortner', function (req, res) {
		UrlController.shorten(req, res);
	});
}