const ENV = require('dotenv').config()
const express = require('express');
const app = express();
const server = require('http').Server(app);
const path = require('path');
const bodyParser = require('body-parser');

// SET DB CONNECTION STRING
const mongoose = require('mongoose');
const mongoDB = ENV.parsed.MONGOINIT+ENV.parsed.DB_USER+':'+ENV.parsed.DB_PASS + '@' + ENV.parsed.DB_HOST + '/' + ENV.parsed.DB_NAME;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(bodyParser.json());

mongoose.connect(mongoDB, { 
    autoReconnect: true, 
    reconnectInterval: 500, 
    poolSize: 10, 
    connectTimeoutMS: 10000, 
    useNewUrlParser: true
    }, function(err, db) {
        console.log(err);
    }
);

mongoose.Promise = global.Promise;
const db = mongoose.connection;

db.on('error', 
    console.error.bind(console, 'MongoDB connection error:')
);

server.listen(ENV.parsed.PORT, function() {
    console.log('Please visit ' + ENV.parsed.DOMAIN_NAME + ':' + ENV.parsed.PORT);
});

process.on('SIGINT', () => {
    console.info('SIGINT signal received.');
    // Stops the server from accepting new connections and finishes existing connections.
    server.close(function(err) {
        if (err) {
            console.error(err)
            process.exit(1)
        }
    })
});

/* REGISTERING WEB ROUTES */
require('./routes/web')(app);
/* REGISTERING API ROUTES */
require('./routes/api')(app);

app.use(express.static('public'));